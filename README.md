# Altana is a Bitcoin Wallet for Android provided by inbitcoin and powered by GreenAddress

<a href="https://play.google.com/store/apps/details?id=it.inbitcoin.altana" target="_blank">
<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="90"/></a>


# Contributing

Altana development happens on [GitHub](https://github.com/).

For more info on Altana, visit the following link:
- [Altana project](https://github.com/inbitcoin/altana-android)